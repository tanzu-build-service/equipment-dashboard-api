INSERT INTO TSA_DEMO.EQUIPMENT_TYPE VALUES(1, 'Largest Box', 'Large Equipment');
INSERT INTO TSA_DEMO.EQUIPMENT_TYPE VALUES(2, 'Medium Sized Box', 'Med. Equipment');
INSERT INTO TSA_DEMO.EQUIPMENT_TYPE VALUES(3, 'Smallest Box', 'Small Equipment');

INSERT INTO TSA_DEMO.CONFIGURATION VALUES(1, 'Least Resistance', 'S', 123);
INSERT INTO TSA_DEMO.CONFIGURATION VALUES(2, 'Divide', 'S', 455);
INSERT INTO TSA_DEMO.CONFIGURATION VALUES(3, 'Brute', 'S', 498);

INSERT INTO TSA_DEMO.EQUIPMENT VALUES(1, 'AS1234');
INSERT INTO TSA_DEMO.EQUIPMENT VALUES(2, 'NR334');
INSERT INTO TSA_DEMO.EQUIPMENT VALUES(3, 'ZZ1554');
