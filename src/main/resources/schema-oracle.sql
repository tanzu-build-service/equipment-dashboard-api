
alter session set "_ORACLE_SCRIPT"=true;
create user TSA_DEMO identified by mysecret;
alter session set current_schema = TSA_DEMO;
alter user TSA_DEMO quota unlimited on USERS;
GRANT UNLIMITED TABLESPACE TO TSA_DEMO;

create table tsa_demo.configuration (id number(19,0) not null, algorithm varchar2(255 char), security_code varchar2(255 char), sensitivity double precision, primary key (id));
create table tsa_demo.equipment (id number(19,0) not null, serial_number varchar2(255 char), primary key (id));
create table tsa_demo.equipment_type (id number(19,0) not null, description varchar2(255 char), equipment_type varchar2(255 char), primary key (id));
create sequence tsa_demo.config_sequence start with 1 increment by  1 NOCACHE;
create sequence tsa_demo.eqpt_sequence start with 1 increment by  1 NOCACHE;
create sequence tsa_demo.eqpt_type_sequence start with 1 increment by  1 NOCACHE;

